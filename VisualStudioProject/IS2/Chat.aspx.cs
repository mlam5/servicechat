﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NoDB
{
	public partial class WebForm2 : System.Web.UI.Page
	{
		String conString = "workstation id=IS3chatdb1.mssql.somee.com;packet size = 4096; user id = au3641_SQLLogin_1; pwd=r2n4db2158;data source = IS3chatdb1.mssql.somee.com; persist security info=False;initial catalog = IS3chatdb1";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Session["prijavljen"] == null || "".Equals(Session["prijavljen"].ToString()))
				Response.Redirect("Login.aspx");
			else
			{
				CurrentUser.Text = Session["prijavljen"].ToString();
				//CurrentUser.Text = "Prijavljeni ste kot " + Session["login_name"].ToString() + ".";
				refresh_users();
				refresh_messages();
				if (Session["is_logged_on"] == null)
				{
					Users.Items.Add(Session["prijavljen"].ToString());
					Application["Users"] = Users.Items.Cast<ListItem>().ToArray();
					Session["is_logged_on"] = true;
				}
			}
		}

		protected void Logout_Click(object sender, EventArgs e)
		{
			refresh_users();
			Users.Items.Remove(Session["prijavljen"].ToString());
			Application["Users"] = Users.Items.Cast<ListItem>().ToArray();
			Session.Remove("prijavljen");
			//Session.Remove("login_user");
			Session.Remove("is_logged_on");
			Response.Redirect("Login.aspx");
		}

		protected void Send_Click(object sender, EventArgs e)
		{
			if (Message.Text.Equals(""))
				return;

			refresh_messages();

			try
			{
				SqlConnection conn = new SqlConnection(conString);
				conn.Open();
				string query = "insert into Pogovor(username, besedilo) values ('" + Session["prijavljen"] + "', '" + Message.Text + "', '" + DateTime.Now + "')";
                //string query2 = "update Uporabnik set st_sporocil = st_sporocil + 1 where username='" + Session["prijavljen"] + "'";
				//string query = "insert into Pogovor(username, besedilo) values (@username, @besedilo)";
				SqlCommand sql = new SqlCommand(query, conn);
                //SqlCommand sql2 = new SqlCommand(query2, conn);
                //sql2.ExecuteNonQuery();
				//sql.Parameters.AddWithValue("@username", Session["prijavljen"].ToString());
				//sql.Parameters.AddWithValue("@besedilo", Message.Text);
				sql.ExecuteNonQuery();
				Messages.Items.Add(Session["prijavljen"] + ": " + Message.Text);
				//Application["Messages"] = Messages.Items.Cast<ListItem>().ToArray();
				Message.Text = "";

				conn.Close();
			}
			catch (Exception exp)
			{
				ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Napaka pri pošiljanju sporočila.\n" + exp.ToString() + "')", true);
			}
		}

		private void refresh_users()
		{
			Users.Items.Clear();
			if(Application["Users"] != null)
				Users.Items.AddRange((ListItem[])Application["Users"]);
		}

		private void refresh_messages()
		{
			SqlConnection conn = new SqlConnection(conString);
			conn.Open();

			string query = "select username, besedilo from Pogovor";
			SqlCommand sql = new SqlCommand(query, conn);

			using (SqlDataReader reader = sql.ExecuteReader())
			{
				Messages.Items.Clear();
				while (reader.Read())
				{
					Messages.Items.Add(reader.GetString(0) + ": " + reader.GetString(1));
					//Messages.Items.AddRange((ListItem[])Application["Messages"]);
				}
			}

			conn.Close();
			//if (Application["Messages"] != null)
		}

		protected void Refresh_Click(object sender, EventArgs e)
		{
			refresh_messages();
			refresh_users();
		}
	}
}