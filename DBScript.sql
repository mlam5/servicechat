CREATE TABLE [dbo].[Pogovor] (
    [id]        INT           IDENTITY (1, 1) NOT NULL,
    [username]  VARCHAR (50)  NOT NULL,
    [besedilo]  VARCHAR (MAX) NOT NULL,
    [timestamp] DATETIME      DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
)

CREATE TABLE [dbo].[Uporabnik] (
    [username] VARCHAR (50) NOT NULL,
    [ime]      VARCHAR (50) NOT NULL,
    [priimek]  VARCHAR (50) NOT NULL,
    [geslo]    VARCHAR (50) NOT NULL,
    [admin]    INT          DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([username] ASC)
)

INSERT INTO [dbo].[Uporabnik] ([username], [ime], [priimek], [geslo], [admin]) VALUES (N'admin', N'Admin', N'Adminovic', N'775D9180BBD7D8354D13116C385E37E8', 1)
INSERT INTO [dbo].[Uporabnik] ([username], [ime], [priimek], [geslo], [admin]) VALUES (N'JakaS', N'Jaka', N'S', N'944061BCB39B63883599EE087215466D', 0)
INSERT INTO [dbo].[Uporabnik] ([username], [ime], [priimek], [geslo], [admin]) VALUES (N'mihalamp5', N'Miha', N'', N'C8455CD235A156739F5D1EA087FB0FBE', 1)
INSERT INTO [dbo].[Uporabnik] ([username], [ime], [priimek], [geslo], [admin]) VALUES (N'testing', N'Test', N'Tester', N'944061BCB39B63883599EE087215466D', 1)
INSERT INTO [dbo].[Uporabnik] ([username], [ime], [priimek], [geslo], [admin]) VALUES (N'Tester', N'a', N'u', N'E5894751261D77555EB4BDE97705ABE0', 0)