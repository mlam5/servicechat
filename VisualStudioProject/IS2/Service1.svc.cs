﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Security.Cryptography;
using System.Net;

namespace IS2
{
	[ServiceContract]
	[AspNetCompatibilityRequirements(RequirementsMode=AspNetCompatibilityRequirementsMode.Allowed)]
	public class Service1 //: IService1
	{
		private string user;

		private bool AuthenticateUser()
		{
			WebOperationContext ctx = WebOperationContext.Current;
			string authHeader = ctx.IncomingRequest.Headers[HttpRequestHeader.Authorization];
			if (authHeader == null)
				return false;

			string[] loginData = authHeader.Split(':');
			if (loginData.Length == 2 && Login(loginData[0], loginData[1]))
			{
				user = loginData[0];
				return true;
			}
			return false;
		}

		private string GetMsgFromHeader()
		{
			IncomingWebRequestContext request = WebOperationContext.Current.IncomingRequest;
			WebHeaderCollection headers = request.Headers;
			string msg = "";

			if (headers["X-msg"] != null)
				msg = headers["X-msg"];

			return msg;
		}

		String conString = "workstation id=IS3chatdb1.mssql.somee.com;packet size = 4096; user id = au3641_SQLLogin_1; pwd=r2n4db2158;data source = IS3chatdb1.mssql.somee.com; persist security info=False;initial catalog = IS3chatdb1";
		//String conString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

		public string MD5Hash(string input)
		{
			// step 1, calculate MD5 hash from input
			MD5 md5 = System.Security.Cryptography.MD5.Create();
			byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
			byte[] hash = md5.ComputeHash(inputBytes);

			// step 2, convert byte array to hex string
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < hash.Length; i++)
				sb.Append(hash[i].ToString("X2"));

			return sb.ToString();

		}

		// To use HTTP GET, add [WebGet] attribute. (Default ResponseFormat is WebMessageFormat.Json)
		// To create an operation that returns XML,
		//     add [WebGet(ResponseFormat=WebMessageFormat.Xml)],
		//     and include the following line in the operation body:
		//         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
		[OperationContract]
		[WebGet(UriTemplate = "Login/{username}/{password}")]
		public bool Login(string username, string password)
		{
			try
			{
				SqlConnection conn = new SqlConnection(conString);
				conn.Open();

				string check = "select count(*) from Uporabnik where username=@username";
				SqlCommand sql = new SqlCommand(check, conn);
				sql.Parameters.AddWithValue("@username", username);
				int x = Convert.ToInt32(sql.ExecuteScalar().ToString());
				conn.Close();

				if (x == 1) //obstaja uporabniško ime 
				{
					conn.Open();
					string checkpass = "select geslo from Uporabnik where username=@username";
					SqlCommand sql2 = new SqlCommand(checkpass, conn);
					sql2.Parameters.AddWithValue("@username", username);
					string pass = sql2.ExecuteScalar().ToString();

					conn.Close();
					//preveri če je geslo pravilno
					if (pass == MD5Hash(password))
					{
						//pridobi ime in priimek iz baze
						conn.Open();
						string ime = "select ime from Uporabnik where username=@username";
						string priimek = "select priimek from Uporabnik where username=@username";
						SqlCommand sql3 = new SqlCommand(ime, conn);
						SqlCommand sql4 = new SqlCommand(priimek, conn);
						sql3.Parameters.AddWithValue("@username", username);
						sql4.Parameters.AddWithValue("@username", username);
						string imeexe = sql3.ExecuteScalar().ToString();
						string priimekexe = sql4.ExecuteScalar().ToString();
						conn.Close();

						//shrani ime v sejno spremenljivko
						return true;
						/*Session["prijavljen"] = imeexe + " " + priimekexe;

						Response.Write(imeexe + " " + priimekexe + "Prijava uspešna!");
						Response.Redirect("Chat.aspx");*/
					}
					else
					{
						return false;
					}
				}
				else //uporabniško ime ne obstaja
				{
					return false;
				}

			}
			catch (Exception ex)
			{
				return false;
			}

		}


		public bool userExists(string username)
		{
			SqlConnection conn = new SqlConnection(conString);
			conn.Open();
			string check = "select count(*) from Uporabnik where username=@username";
			SqlCommand sql = new SqlCommand(check, conn);
			sql.Parameters.AddWithValue("@username", username);
			int x = Convert.ToInt32(sql.ExecuteScalar().ToString());
			conn.Close();

			if (x == 1) //obstaja uporabniško ime 
				return true;
			return false;
		}

		[OperationContract]
		[WebInvoke(UriTemplate = "Send")]
		public bool Send()
		{
			string msg = GetMsgFromHeader();

			if (msg.Equals(""))
				return false;

			if (!AuthenticateUser())
				return false;

			try
			{
				SqlConnection conn = new SqlConnection(conString);
				conn.Open();
				string query = "insert into Pogovor(username, besedilo, timestamp) values (@username, @besedilo, @timestamp)";
					//" + Session["prijavljen"] + "', '" + Message.Text + "', '" + DateTime.Now + "')";
				SqlCommand sql = new SqlCommand(query, conn);

				sql.Parameters.AddWithValue("@username", user);
				sql.Parameters.AddWithValue("@besedilo", msg);
				sql.Parameters.AddWithValue("@timestamp", DateTime.Now);

				bool end = sql.ExecuteNonQuery() == 1;
				conn.Close();

				return end;				
			}
			catch (Exception exp)
			{
				return false;
			}
		}

		[OperationContract(Name = "AllMsg")]
		[WebGet(UriTemplate = "Messages")]
		public List<Message> Messages()
		{
			if (!AuthenticateUser())
				return new List<Message> { };

			String query = "SELECT * FROM Pogovor";
			List<Message> messages = new List<Message> { };
			SqlConnection con = new SqlConnection(conString);
			SqlCommand myCommand = new SqlCommand(query, con);
			con.Open();
			SqlDataReader reader = myCommand.ExecuteReader();
			
			while (reader.Read())
				messages.Add(new Message {id = reader.GetInt32(0).ToString(), usr = reader.GetString(1), msg = reader.GetString(2), date = reader.GetDateTime(3).ToString() });

			con.Close();

			return messages;
		}

		[OperationContract(Name = "SomeMsg")]
		[WebGet(UriTemplate = "Messages/{id}")]
		public List<Message> Messages(string id)
		{
			if (!AuthenticateUser())
				return new List<Message> { };

			String query = "SELECT * FROM Pogovor where id > @id";
			List<Message> messages = new List<Message> { };
			SqlConnection con = new SqlConnection(conString);
			SqlCommand sql = new SqlCommand(query, con);
			sql.Parameters.AddWithValue("@id", id);
			con.Open();
			SqlDataReader reader = sql.ExecuteReader();

			while (reader.Read())
				messages.Add(new Message { id = reader.GetInt32(0).ToString(), usr = reader.GetString(1), msg = reader.GetString(2), date = reader.GetDateTime(3).ToString() });

			con.Close();

			return messages;
		}
	}

	[DataContract]
	public class Message
	{
		public Message(string id, string usr, string msg, string date)
		{
			this.id = id;
			this.msg = msg;
			this.usr = usr;
			this.date = date;
		}

		public Message()
		{ }

		[DataMember]
		public string id { get; set; }

		[DataMember]
		public string usr { get; set; }

		[DataMember]
		public string msg { get; set; }

		[DataMember]
		public string date { get; set; }
	}
}
