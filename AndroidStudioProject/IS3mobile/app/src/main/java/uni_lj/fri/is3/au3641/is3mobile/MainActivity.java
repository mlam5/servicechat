package uni_lj.fri.is3.au3641.is3mobile;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class MainActivity extends Activity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    private String usr = "";
    private String pass = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void onLoginClick(View view)
    {
        usr = ((EditText)findViewById(R.id.usernameText)).getText().toString();
        pass = ((EditText)findViewById(R.id.passwordText)).getText().toString();

        final RESTCallTask rest = new RESTCallTask();
        rest.execute(usr, pass);
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    public class RESTCallTask extends AsyncTask<String, Void, String>
    {
        private final String SERVICE_URL = "http://is3.somee.com/IS2/Service1.svc/Login/%s/%s";

        @Override
        protected String doInBackground(String... params)
        {
            String callURL = String.format(SERVICE_URL, params[0], params[1]);

            DefaultHttpClient hc = new DefaultHttpClient();
            String result = "";

            try
            {
                HttpGet request = new HttpGet(callURL);
                request.setHeader("Accept", "application/xml");

                HttpResponse response = hc.execute(request);
                HttpEntity httpEntity = response.getEntity();
                result = EntityUtils.toString(httpEntity);
                Log.i("b", result);
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result)
        {
            result = parseXml(result);

            if(result.equals("true") && !usr.equals(""))
            {
                ((TextView)findViewById(R.id.textError)).setVisibility(View.INVISIBLE);
                //call chat activity
                Intent intent = new Intent(MainActivity.this, ChatActivity.class);
                intent.putExtra("username", usr);
                intent.putExtra("password", pass);
                startActivity(intent);
                finish();
            }
            else
            {
                ((TextView)findViewById(R.id.textError)).setVisibility(View.VISIBLE);
            }
        }

        private String parseXml(String input)
        {
            String output = "";
            try
            {
                Document doc = convertStringToDocument(input);
                doc.getDocumentElement().normalize();

                NodeList nList = doc.getElementsByTagName("boolean");

                for (int temp = 0; temp < nList.getLength(); temp++)
                {
                    Node node = nList.item(temp);

                    if (node.getNodeType() == Node.ELEMENT_NODE)
                    {
                        Element element = (Element) node;
                        output = node.getTextContent();

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return output;
        }

        private Document convertStringToDocument(String xmlStr) {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            try
            {
                builder = factory.newDocumentBuilder();
                return builder.parse( new InputSource( new StringReader( xmlStr ) ) );
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}