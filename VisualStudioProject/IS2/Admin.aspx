﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="IS2.UsersManager" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 143px;
        }
        .auto-style3 {
            height: 23px;
        }
        .auto-style4 {
            height: 143px;
            text-align: center;
            width: 482px;
        }
        .auto-style5 {
            height: 23px;
            width: 482px;
            text-align: center;
        }
        .auto-style6 {
            text-align: right;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="auto-style6">
    
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Uporabnik]" ></asp:SqlDataSource>
    
        <asp:Button ID="Odjava_admin" runat="server" OnClick="Odjava_admin_Click" Text="Odjava" />
    
    </div>
        <table class="auto-style1">
            <tr>
                <td class="auto-style4">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="username" Height="139px" Width="657px" CellPadding="4" ForeColor="Black" GridLines="Horizontal" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
            <Columns>
                <asp:BoundField DataField="username" HeaderText="username" ReadOnly="True" SortExpression="username" />
                <asp:BoundField DataField="ime" HeaderText="ime" SortExpression="ime" />
                <asp:BoundField DataField="priimek" HeaderText="priimek" SortExpression="priimek" />
                <asp:BoundField DataField="admin" HeaderText="admin" SortExpression="admin" />
            </Columns>
            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BorderColor="Red" BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
            <SortedDescendingCellStyle BackColor="#E5E5E5" />
            <SortedDescendingHeaderStyle BackColor="#242121" />
        </asp:GridView>
                </td>
                <td class="auto-style2">
                    <asp:ListBox ID="ListBox1" runat="server" DataTextField="username" DataValueField="username" Height="249px" Width="534px"></asp:ListBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style5">
                    &nbsp;</td>
                <td class="auto-style3">
                    <asp:Label ID="Label2" runat="server" Text="Admin"></asp:Label>
                    <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Da/Ne" />
                    <asp:Button ID="Button4" runat="server" OnClick="Button4_Click" Text="Prikaži" />
                    <asp:Label ID="Label3" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style5">
                </td>
                <td class="auto-style3">Število sporočil:
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                    <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Prikaži" />
                </td>
            </tr>
            <tr>
                <td class="auto-style5">
                </td>
                <td class="auto-style3">
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Izbriši uporabnika" />
                </td>
            </tr>
        </table>
        <p>
            &nbsp;</p>
    </form>
</body>
</html>
