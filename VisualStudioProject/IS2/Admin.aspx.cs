﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IS2
{
    public partial class UsersManager : System.Web.UI.Page
    {
		String conString = "workstation id=IS3chatdb1.mssql.somee.com;packet size = 4096; user id = au3641_SQLLogin_1; pwd=r2n4db2158;data source = IS3chatdb1.mssql.somee.com; persist security info=False;initial catalog = IS3chatdb1";

		protected void Page_Load(object sender, EventArgs e)
        {
			if (Session["prijavljen"] == null || "".Equals(Session["prijavljen"].ToString()))
				Response.Redirect("Login.aspx");

			if (!IsPostBack)
			{
				SqlConnection con = new SqlConnection(conString);
				con.Open();
				SqlCommand cmd = con.CreateCommand();
				cmd.CommandType = CommandType.Text;
				cmd.CommandText = "select * from Uporabnik";
				SqlDataAdapter da = new SqlDataAdapter(cmd);
				DataTable dt = new DataTable();
				da.Fill(dt);
				GridView1.DataSource = dt;
				GridView1.DataBind();

				SqlCommand cmd2 = con.CreateCommand();
				cmd2.CommandType = CommandType.Text;
				cmd2.CommandText = "select username from Uporabnik";
				SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
				DataTable dt2 = new DataTable();
				da2.Fill(dt2);
				ListBox1.DataSource = dt2;
				ListBox1.DataBind();

				con.Close();
			}
		}

      
		/*
        protected void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string username = ListBox1.SelectedItem.ToString();
            //var st_sporocil = 0;
            //var admin = 0;


            //SqlConnection conn = new SqlConnection(conString);
            //conn.Open();

            //SqlCommand cmd = new SqlCommand("Select count(*) from Pogovor where username like @username ", conn);
            //cmd.Parameters.AddWithValue("@username", username);
            //SqlCommand cmd2 = new SqlCommand("Select admin from Uporabnik where username like @username", conn);
            //cmd2.Parameters.AddWithValue("@username", username);

            //st_sporocil = (Int32)cmd.ExecuteScalar();
            //admin = (Int32)cmd2.ExecuteScalar();
            ////s
            //conn.Close();



        }*/

        // IZBRIŠI UPORABNIKA
        protected void Button1_Click(object sender, EventArgs e)
        {

            if(ListBox1.SelectedIndex != -1)
            {
                string username = ListBox1.SelectedItem.ToString();

                SqlConnection conn = new SqlConnection(conString);
                conn.Open();

                SqlCommand cmd = new SqlCommand("delete from Pogovor where username like @username", conn);
                SqlCommand cmd2 = new SqlCommand("delete from Uporabnik where username like @username", conn);
                cmd.Parameters.AddWithValue("@username", username);
                cmd2.Parameters.AddWithValue("@username", username);

                cmd.ExecuteNonQuery();
                cmd2.ExecuteNonQuery();

                conn.Close();

                ListBox1.ClearSelection();
                ListBox1.DataBind();

            }

        }

        // NASTAVI/ODSTRANI ADMINIASTRATORJA
        protected void Button2_Click(object sender, EventArgs e)
        {
            if (ListBox1.SelectedIndex != -1)
            {
                
                string username = ListBox1.SelectedItem.ToString();
                var admin=0;
                SqlConnection conn = new SqlConnection(conString);
                conn.Open();
                SqlCommand cmd2 = new SqlCommand("Select admin from Uporabnik where username like @username", conn);
                cmd2.Parameters.AddWithValue("@username", username);
                admin = (Int32)cmd2.ExecuteScalar();
                conn.Close();



                if (admin == 1)
                {
                    Label3.Text = "NE";
                    SqlConnection conn2 = new SqlConnection(conString);
                    conn2.Open();
                    SqlCommand cmd3 = new SqlCommand("Update Uporabnik set admin=@admin where username like @username", conn2);
                    cmd3.Parameters.AddWithValue("@username", username);
                    cmd3.Parameters.AddWithValue("@admin", 0);
                    cmd3.ExecuteScalar();
                    conn2.Close();
                }

                else if (admin == 0)
                {
                    Label3.Text = "DA";
                    SqlConnection conn3 = new SqlConnection(conString);
                    conn3.Open();
                    SqlCommand cmd4 = new SqlCommand("Update Uporabnik set admin=@admin where username like @username", conn3);
                    cmd4.Parameters.AddWithValue("@username", username);
                    cmd4.Parameters.AddWithValue("@admin", 1);
                    cmd4.ExecuteScalar();
                    conn3.Close();

                }

              
            }

            

        }


        //PRIKAŽI ŠTEVILO SPOROČIL UPORABNIKA
        protected void Button3_Click(object sender, EventArgs e)
        {
			string usr1 = ListBox1.SelectedItem.Value.ToString();
			string username = ListBox1.SelectedItem.ToString();
            var st_sporocil = 0;
            


            SqlConnection conn = new SqlConnection(conString);
            conn.Open();


            SqlCommand cmd2 = new SqlCommand("Select ime from Uporabnik where username like @username", conn);
            
            cmd2.Parameters.AddWithValue("@username", username);
            
            String ime = (string)cmd2.ExecuteScalar();
            //Label4.Text = ime;
            SqlCommand cmd = new SqlCommand("Select count(*) from Pogovor where username like @username ", conn);
            cmd.Parameters.AddWithValue("@username", ime);
            st_sporocil = (Int32)cmd.ExecuteScalar();

            Label1.Text = st_sporocil.ToString();

            //s
            conn.Close();
        }


        //PRIKAŽI ALI JE ADMIN
        protected void Button4_Click(object sender, EventArgs e)
        {
            if (ListBox1.SelectedIndex != -1)
            {

                string username = ListBox1.SelectedItem.ToString();
                var admin = 0;
                SqlConnection conn = new SqlConnection(conString);
                conn.Open();
                SqlCommand cmd2 = new SqlCommand("Select admin from Uporabnik where username like @username", conn);
                cmd2.Parameters.AddWithValue("@username", username);
                admin = (Int32)cmd2.ExecuteScalar();
                conn.Close();


                if (admin == 1)
                {
                    Label3.Text = "DA";
                }
                else
                    Label3.Text = "NE";
            }

        }

        protected void Odjava_admin_Click(object sender, EventArgs e)
        {
            
            
            
            Session.Remove("prijavljen");
            //Session.Remove("login_user");
            Session.Remove("is_logged_on");
            Response.Redirect("Login.aspx");
        }
    }
}