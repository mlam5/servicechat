# README #

Informacijski sistemi  
3. seminarska naloga  
##**ServiceChat**##

Miha Lampret (63130131)  
Andrej Ulčar (63130266)

Spletna aplikacija in spletni vmesnik sta na voljo na:  
http://is3.somee.com/IS2/Login.aspx  
http://is3.somee.com/IS2/Service1.svc

####**Kratek opis delovanja/težav pri izdelavi aplikacije/izboljšav**####

######**Administratorski vmesnik**######

*Število sporočil uporabnika* - 
V podatkovni bazi se v tabelo Pogovor pod entiteto username shanjuje ime uporabnika, ki pošilja sporočilo.(v tabelo Uporabnik pa se pod username shranjuje uporabniško ime uporabnika)
Za seštevek sporočil določenega uporabnika sem zato uporabil poizvedbo:
"Select ime from Uporabnik where username like username",
ki proizvede ime izbranega uporabnika in nato uporabil poizvedbo:
"Select count(*) from Pogovor where username like ime ",
ki v tabeli Pogovor sešteje vsa sporočila uporabnika z imenom "ime".

*Ali je uporabnik administrator* - 
pri prikazu podatkov registriranih uporabnikov sem imel težave z uporabo DataGridView, saj se ob spremembi atributov GridView ni posodobil. 
Zato sem uporabil Label in gumb s katerim se prikaže lastnost uporabnika(ali je administrator ali ne).
Z dodatnim gumbom pa spreminjamo ali je uporabnik administrator ali ne.

*Prijava administratorja* - 
Prijavo sem implementiral podobno kot pri navadnih uporabnikih. V tabelo Uporabnik sem dodal lastnost admin, ki je lahko 1(uporabik je admin) ali 0(uporabnik ni admin)
Pri prijavi administratorja sem tako dodal pogoj, ki preverja ali ima uporabnik, ki se skuša prijaviti admin=1.

######**.NET REST storitev**######

Storitev deluje po podanih specifikacijah. Uporabniško ime in geslo se vedno pošiljata preko zglavja. Izjema je funkcija Login, ki ju sprejme preko naslovne poti. Nekaj posebnosti je tudi pri funkciji Send. Sporočilo se pošilja preko zglavja, zaradi težav, ki so nastale pri izdelavi android aplikacije. Storitev pa je z izjemo podatkovne baze, popolnoma neodvisna od preostale spletne aplikacije. 

Edino težavo na katero sem naletel pri izdelavi storitve je bila povezava na oddaljeno podatkovno bazo. Povezave ni bilo moč vzpostaviti preko grafičnega vmesnika, zato povezavo vzpostavim preko vnaprej določenega niza.

Izboljšave:

- Trenutno storitev pričakuje nekodirano geslo in le-tega za primerjavo z bazo zakodira šele na strežniku. Kodiranje gesel bi morali prestaviti iz strežnika na klienta.

######**Android aplikacija**######

Android aplikacija je sestavljena iz dveh aktivnosti. Prijavne aktivnosti (MainActivity) in pogovorne aktivnosti (ChatActivity). Prijavna aktivnost je prva, ki jo uporabnik vidi, ko odpre aplikacijo. Tu lahko uporabnik vnese svoje uporabniško ime in geslo. V primeru nepravilnega vnosa, ga aplikacija na to opozori. Prijavo, aplikacija ob pritisku na gumb 'Prijava', preveri preko vmesnika s pomočjo funkcije Login. Ob uspešni prijavi, aplikacija preklopi na pogovorno aktivnost, hkrati pa ji pošlje podatke o uporabniku.
Pogovorna aktivnost, že ob nalaganju osveži listo sporočil, prav tako pa tudi ob pošiljanju sporočila in ob pritisku na gumb osveži. V tej aktivnosti je moč videti sporočila vseh uporabnikov. Aktivnost posodobi sporočila le ob zahtevi uporabnika, in tudi takrat iz strežnika zahteva le nova sporočila. To vse opravlja s pomočjo REST storitve. Prav tako uporablja funkcije le-te, za pošiljanje sporočila.

Težave pri android aplikaciji sem imel s pošiljanjem sporočil. Sprva je za vsak poseben znak strežnik javil napako. Poskusil sem z različnimi načini kodiranja sporočila, vendar sem tako lahko usposobil le nekatere znake, vendar ne vseh. Končno sem problem rešil tako, da sem pošiljanje vseh sporočil prenesel v zglavje http zahteve.

Izboljšave:

- Trenutno storitev pričakuje nekodirano geslo in le-tega za primerjavo z bazo zakodira šele na strežniku. Kodiranje gesel bi morali prestaviti iz strežnika na klienta.
- Osveževanje sporočil bi lahko preverjali avtomatsko na določeno časovno periodo.
- Izgled ni narejen po android standardih (material design).


####**Opis nalog, ki jih je izvedel vsak izmed študentov**####

Miha Lampret: Podatkovna baza, Administratorski spletni vmesnik  
Andrej Ulčar: .NET REST storitev, postavitev somee strežnika, android aplikacija

####**Seznam treh uporabnikov (uporabniško ime/geslo), ki so shranjeni v podatkovni bazi projekta in se lahko prijavijo v aplikacijo.**####

Uporabniško ime: JakaS  
Geslo: JJakogeslo12!

Uporabniško ime: admin  
Geslo: NNepovem12!

Uporabniško ime: Tester  
Geslo: TEst.001


####**Slika podatkovnega modela podatkovne baze z opisom.**####
![Screenshot 2017-01-16 16.27.34.png](https://bitbucket.org/repo/reA4Rg/images/2415499750-Screenshot%202017-01-16%2016.27.34.png)

V tabeli Pogovor imamo podatke o sporočilih. Imamo ID sporočila, username – uporabniško ime uporabnika ki je oddal sporočilo, timestamp – čas oddaje ter besedilo, ki je dejansko sporočilo
V tabeli Uporabniki pa je username – uporabniško ime registriranega uporabnika, ime in priimek, geslo, ki je shranjeno v MD5 obliki ter admin, ki pove ali je uporabnik admin ali ne(1 ali 0).

####**Zaslonska slika grafičnega vmesnika mobilne aplikacije in administratorskega vmesnika**####

######Administratorski spletni vmesnik######
![alt text](https://bitbucket.org/repo/reA4Rg/images/1602173866-Admin%20screenshot.PNG "Administracija")
######Android - Prijavni zaslon######
![alt text](./AndroidLogin.PNG "Android - prijavni zaslon")
######Android - Klepetalnica######
![alt text](./AndroidChat.PNG "Android - klepetalnica")