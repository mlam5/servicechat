﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="IS2.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
		.buttons{
			border-radius: 5px;
			width: 70%;
			padding-left: 0;
			padding-right: 0;
			margin-left: 0;
			margin-right: 0;
		}
		.buttons:active{
			border-radius: 5px;
			background-color: #aaa;
			width: 70%;
			padding-left: 0;
			padding-right: 0;
			margin-left: 0;
			margin-right: 0;
		}

		.input_fields{
			width: 70%;
			padding-left: 0;
			padding-right: 0;
			margin-left: 0;
			margin-right: 0;
		}
        .auto-style1 {
            width: 100%;
            height: 281px;
            margin-right: 248px;
            margin-bottom: 0px;
        }
        .auto-style24 {
            width: 2963px;
            }
        .auto-style67 {
            height: 30px;
            width: 2963px;
            text-align: right;
        }
        .auto-style68 {
            height: 30px;
            width: 2414px;
        }
        .auto-style69 {
            height: 30px;
            width: 1223px;
        }
        .auto-style73 {
            width: 2963px;
            text-align: right;
            height: 1px;
        }
        .auto-style74 {
            width: 2414px;
            height: 1px;
        }
        .auto-style75 {
            width: 1223px;
            height: 1px;
        }
        .auto-style81 {
            width: 25%;
            text-align: right;
            height: 31px;
            font-size: small;
        }
        .auto-style82 {
            width: 25%;
            height: 31px;
        }
        .auto-style96 {
            width: 845px;
            height: 518px;
        }
        .auto-style97 {
            width: 2414px;
        }
        .auto-style98 {
            width: 1223px;
        }
        .auto-style100 {
            width: 25%;
            height: 31px;
            font-size: small;
        }
        .auto-style101 {
            width: 2560px;
            height: 1px;
            text-align: right;
        }
        .auto-style102 {
            width: 2560px;
            height: 30px;
            text-align: right;
        }
        .auto-style104 {
            height: 30px;
            width: 2560px;
        }
        .auto-style107 {
            width: 2560px;
        }
    	.auto-style109 {
			width: 50%;
			height: 50px;
		}
		.auto-style110 {
			width: 50%;
		}
        .auto-style111 {
            border-radius: 5px;
            padding-left: 0;
            padding-right: 0;
            margin-left: 0;
            margin-right: 0;
        }
    </style>
</head>
<body>
   
    <form id="form1" runat="server" style="margin-right:auto; margin-left:auto; padding-top: 0px; border:1px solid #000;"  class="auto-style96">
		<table style="width: 100%; text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: xx-large; font-weight: bolder;">
			<tr>
				<td style="padding-bottom: 60px; padding-top: 30px;">ChatDB</td>
			</tr>
		</table>
		<table class="auto-style2" style="margin: 0px; width: 100%; height: 100px;">
			<tr>
				<td style="text-align:center; font-size: x-large; font-weight: bold; font-family: Helvetica, Arial, sans-serif; font-style: normal; font-variant: normal; margin-bottom: 0px; margin-top: 0px; padding-bottom: 0px; padding-top: 0px; vertical-align: bottom;" class="auto-style109">Registracija</td>
				<td style="text-align:center; font-size: x-large; font-weight: bold; font-family: Helvetica, Arial, sans-serif; font-style: normal; font-variant: normal; margin-top: 0px; margin-bottom: 0px; vertical-align: bottom;" class="auto-style109">Prijava</td>
			</tr>
			<tr>
				<td style="text-align:center; font-family: Helvetica, Arial, sans-serif; font-size: medium; font-weight: bold; font-style: italic; color: #999999; margin-top: 0px; padding-top: 6px; vertical-align: top;" class="auto-style110">Novi uporabniki</td>
				<td style="text-align:center; font-family: Helvetica, Arial, sans-serif; font-size: medium; font-weight: bold; font-style: italic; color: #999999; margin-top: 0px; padding-top: 6px; vertical-align: top;" class="auto-style110">Obstoječi uporabniki</td>
			</tr>
		</table>
        <table class="auto-style1" style="margin: 0px">
			<tr>
                <td class="auto-style81" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; height: 10px;"></td>
                <td class="auto-style82" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; height: 10px;"></td>
                <td class="auto-style100" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; height: 10px;"></td>
                <td class="auto-style82" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; height: 10px;"></td>
            </tr>
            <tr>
                <td class="auto-style73" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; height: 30px; padding-right: 12px;">Ime:</td>
                <td class="auto-style74" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; height: 30px;"><asp:TextBox ID="ImeTB" runat="server" CssClass="input_fields"></asp:TextBox>
                </td>
                <td class="auto-style101" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; height: 30px;  padding-right: 12px;">Uporabniško ime:</td>
                <td class="auto-style75" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; height: 30px;">
                    <asp:TextBox ID="TextBoxUsername" runat="server" CssClass="input_fields"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style67" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; padding-right: 12px;">Uporabniško ime:</td>
                <td class="auto-style68" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; "><asp:TextBox ID="UporabniškoImeTB" runat="server" CssClass="input_fields"></asp:TextBox>
                </td>
                <td class="auto-style102" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; padding-right: 12px;">Geslo:</td>
                <td class="auto-style69" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; ">
                    <asp:TextBox ID="TextBoxPassword" runat="server" TextMode="Password" CssClass="input_fields"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style67" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; padding-top: 0px; padding-bottom: 0px; padding-right: 12px;">Geslo:</td>
                <td class="auto-style68" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; padding-top: 0px; padding-bottom: 0px;"><asp:TextBox ID="Geslo1TB" TextMode="Password"  runat="server" CssClass="input_fields"></asp:TextBox>
                    </td>
                <td class="auto-style104" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; padding-top: 0px; padding-bottom: 0px;">
                    <br />
                </td>
                <td class="auto-style69" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; padding-top: 0px; padding-bottom: 0px;"></td>
            </tr>
            <tr>
                <td class="auto-style67" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; padding-right: 12px;">Geslo:</td>
                <td class="auto-style68" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px;"><asp:TextBox ID="Geslo2TB" TextMode="Password" runat="server" CssClass="input_fields"></asp:TextBox>
                </td>
                <td class="auto-style104" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px;"></td>
                <td class="auto-style69" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px;">
                    <asp:Button ID="ButtonPrijava" runat="server" OnClick="ButtonPrijava_Click" Text="Prijava" BorderColor="#999999" BorderStyle="Solid" BorderWidth="2px" Height="26px" CssClass="buttons" />
                    <br />
                </td>
            </tr>
            <tr>
                <td class="auto-style67" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; ">
                    </td>
                <td class="auto-style68">
					<asp:Label ID="Napake" runat="server" ForeColor="Red"></asp:Label>
                    <asp:Label ID="Success" runat="server" ForeColor="#33CC33"></asp:Label>
            
                </td>
                <td class="auto-style104" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; "></td>
                <td class="auto-style69" style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; ">
                    <asp:Button ID="ButtonPrijavaAdmin" runat="server" OnClick="ButtonPrijavaAdmin_Click" Text="Prijava v administratorske strani" BorderColor="#999999" BorderStyle="Solid" BorderWidth="2px" Height="26px" CssClass="auto-style111" Width="200px" />
                    </td>
            </tr>
            <tr>
                <td class="auto-style67"></td>
                <td class="auto-style68"  style="font-family: Arial, Helvetica, sans-serif; margin-top: 0px; margin-bottom: 0px; ">
                    <asp:Button ID="ButtonReg" runat="server" Text="Registracija" OnClick="ButtonReg_Click" BorderColor="#999999" BorderStyle="Solid" BorderWidth="2px" CssClass="buttons" />
                </td>
                <td class="auto-style104"></td>
                <td class="auto-style69"></td>
            </tr>
            <tr>
                <td class="auto-style24"></td>
                <td class="auto-style97"></td>
                <td class="auto-style107"></td>
                <td class="auto-style98"></td>
            </tr>
        </table>   
    </form>
   
</body>
</html>
