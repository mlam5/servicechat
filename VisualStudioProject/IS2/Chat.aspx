﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Chat.aspx.cs" Inherits="NoDB.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<style type="text/css">
		table,tr,td{
			padding: 5px;
			border-collapse: collapse;

		}
		.outter_rows{
			height: 35px;
		}
		.buttons{
			width: 100%;
			height: 35px;
			border-radius: 5px;
			border: 1px solid #000;
		}

		.buttons:active{
			width: 100%;
			height: 35px;
			border-radius: 5px;
			border: 1px solid #000;
			background-color: #aaa;
		}

		.message_special_margin_table{
			width: 100%;
			height: 100%;
			padding: 0;
			margin: 0;
			border: 1px hidden white;
			border-collapse: collapse;
		}

		.message{
			width: 100%;
			height: 35px;
			border: 1px solid #000;
		}

		#form1 {
			width: 60%;
			height: 100%;
			min-width: 500px;
			max-width: 1280px;
			max-height: 1280px;
			min-height: 500px;
			text-align: center;
		}

		.lists{
			width: 100%;
			height: 100%;
			border: 1px solid black;
			border-collapse: collapse;
			padding: 5px 5px 5px 5px;
		}

		.users{
			height: 100%;
		}
		.auto-style1 {
			width: 80%;
			height: 100%;
		}
		.auto-style2 {
			width: 17%;
			height: 100%;
		}

		html{
			height: 500px;
			padding: 0;
			margin: 0;
		}

		body,center{
			min-height: 500px;
			height: 500px;
			padding: 0;
			margin: 0;
			/*height: 500px;*/
		}
	</style>
</head>
<body>
	<center>
		<form id="form1" runat="server" defaultbutton="Send">
			<table style="text-align: left; width: 100%; height: 100%;">
				<tr class="outter_rows">
					<td style="width: 80%">
						<asp:Label ID="CurrentUser" runat="server" Text="Neznanec!"></asp:Label>
					</td>
					<td style="width: 17%">
						<asp:Button ID="Logout" runat="server" OnClick="Logout_Click" style="text-align: center" Text="Odjava" CssClass="buttons" />
					</td>
				</tr>
				<tr>
					<td class="auto-style1">
						<asp:ListBox ID="Messages" runat="server" Height="100%" Width="100%" CssClass="lists"></asp:ListBox>
					</td>
					<td style="padding: 0px; " class="auto-style2">
						<table style="padding: 0px; margin: 0px; width: 100%; height: 100%; border-collapse: collapse; border-spacing: 0px;">
							<tr style="height: 100%">
								<td>
									<asp:ListBox ID="Users" runat="server" Width="100%" CssClass="lists"></asp:ListBox><br />
								</td>
							</tr>
							<tr class="outter_rows">
								<td>
									<asp:Button ID="Refresh" runat="server" OnClick="Refresh_Click" Text="Osveži" Width="100%" CssClass="buttons" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="outter_rows">
					<td style="width: 80%">
						<table class="message_special_margin_table">
							<tr>
								<td style="padding: 0; padding-right: 4px;">
									<asp:TextBox ID="Message" runat="server" Height="30px" CssClass="message"></asp:TextBox>
								</td>
							</tr>
						</table>
					</td>
					<td style="width: 17%">
						<asp:Button ID="Send" runat="server" OnClick="Send_Click" Text="Pošlji" CssClass="buttons" Height="35px" />
					</td>
				</tr>
			</table>
		</form>
	</center>
</body>
</html>
