﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IS2
{
    public partial class Login : System.Web.UI.Page
    {
		String conString = "workstation id=IS3chatdb1.mssql.somee.com;packet size = 4096; user id = au3641_SQLLogin_1; pwd=r2n4db2158;data source = IS3chatdb1.mssql.somee.com; persist security info=False;initial catalog = IS3chatdb1";

		protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                Napake.Text = "";
                //preveri če vpisano uporabniško ime(pri registraciji) že obstaja v bazi
                SqlConnection conn = new SqlConnection(conString);
                conn.Open();

                string zeobstaja = "select count(*) from Uporabnik where username='" + UporabniškoImeTB.Text + "'";
                SqlCommand sql = new SqlCommand(zeobstaja,conn);
                int x = Convert.ToInt32(sql.ExecuteScalar().ToString());

                if (x == 1) //obstaja 
                {
                    //Response.Write("Uporabnik že obstaja.");
                }

                conn.Close();
            }
        }

        //Hash funkcija
        public string MD5Hash(string input)
        {
			// step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
			StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
		}



        protected void ButtonReg_Click(object sender, EventArgs e)
        {

            
            if (!string.IsNullOrWhiteSpace(preveriVnosRegistracija())) 
            {
                Napake.Text = preveriVnosRegistracija();
            }

            else{

                try
                {
                    SqlConnection conn = new SqlConnection(conString);
                    conn.Open();

                    string insert = "insert into Uporabnik (username,ime,priimek,geslo) values (@username,@ime,@priimek,@geslo)";
                    SqlCommand sql = new SqlCommand(insert, conn);
                    sql.Parameters.AddWithValue("@username", UporabniškoImeTB.Text);

                    string[] imepriimek = ImeTB.Text.Split(' ');
                    sql.Parameters.AddWithValue("@ime", imepriimek[0]);

					if (imepriimek.Length >= 2)
						sql.Parameters.AddWithValue("@priimek", imepriimek[1]);
					else
						sql.Parameters.AddWithValue("@priimek", "");

                    sql.Parameters.AddWithValue("@geslo", MD5Hash(Geslo1TB.Text));

                    sql.ExecuteNonQuery();

                    Success.Text = "Vaš račun je uspešno ustvarjen!";

					ImeTB.Text = "";
					UporabniškoImeTB.Text = "";

                    conn.Close();
                }
                catch (Exception ex)
                {
                    Napake.Text = "Uporabnik že obstaja.";
                    //Response.Write("Napaka: " + ex.ToString());
                }

            }
            
        }

        private string preveriVnosRegistracija()
        {
            
            StringBuilder napaka = new StringBuilder();
            if (string.IsNullOrWhiteSpace(ImeTB.Text))
            {
                napaka.Append("Vnesi ime." + Environment.NewLine);
            }

            if (string.IsNullOrWhiteSpace(UporabniškoImeTB.Text))
            {
                napaka.Append("Vnesi uporabniško ime." + Environment.NewLine);
            }

			//var regex = @"^(?=(.*\d){2})(.*?[A-Z]){2,}(?=.*[?.*!:])[0-9a-zA-Z?.*!:]{8,}";
			var regex = @"((?=(.*\d){2,})(?=(.*[A-Z]){2,})(?=.*[?.*!:]).{8,})";
            var validgeslo = Regex.Match(Geslo1TB.Text, regex);
            
            if (!validgeslo.Success)
            {
                napaka.Append("Geslo more vsebovati vsaj 2 veliki črki," + Environment.NewLine + "1 poseben znak(?.*!:)," + Environment.NewLine + " vsaj 2 števki in mora biti dolgo vsaj 8 znakov." + Environment.NewLine);
            }
          
 

            if (Geslo1TB.Text != Geslo2TB.Text)
            {

                napaka.Append("Gesli se morata ujemati.");
            }

            

            return napaka.ToString();

        }

        protected void ButtonPrijava_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection conn = new SqlConnection(conString);
                conn.Open();

                string check="select count(*) from Uporabnik where username='"+ TextBoxUsername.Text + "'";
                SqlCommand sql = new SqlCommand(check, conn);
                int x = Convert.ToInt32(sql.ExecuteScalar().ToString());
                conn.Close();

                if (x == 1) //obstaja uporabniško ime 
                {
                    conn.Open();
                    string checkpass = "select geslo from Uporabnik where username='" + TextBoxUsername.Text + "'";
                    SqlCommand sql2 = new SqlCommand(checkpass, conn);
                    string pass = sql2.ExecuteScalar().ToString();
                    
                    conn.Close();
                    //preveri če je geslo pravilno
                    if (pass == MD5Hash(TextBoxPassword.Text))
                    {
                        //pridobi ime in priimek iz baze
                        conn.Open();
                        string ime = "select ime from Uporabnik where username='" + TextBoxUsername.Text + "'";
                        string priimek = "select priimek from Uporabnik where username='" + TextBoxUsername.Text + "'";
                        SqlCommand sql3 = new SqlCommand(ime, conn);
                        SqlCommand sql4 = new SqlCommand(priimek, conn);
                        string imeexe = sql3.ExecuteScalar().ToString();
                        string priimekexe = sql4.ExecuteScalar().ToString();
                        conn.Close();

                        //shrani ime v sejno spremenljivko
                        Session["prijavljen"] = imeexe + " "+ priimekexe;

                        Response.Write(imeexe + " " + priimekexe + "Prijava uspešna!");
                        Response.Redirect("Chat.aspx");
                    }
                    else
                    {
                        Napake.Text = "Napačno geslo.";
                    }
                }

                //uporabniško ime ne obstaja
                else
                {
                    Napake.Text="Uporabnik ne obstaja.";
                }

            }
            catch (Exception ex)
            {
                Response.Write("Napaka: " + ex.ToString());
            }
        }


        protected void ButtonPrijavaAdmin_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection conn = new SqlConnection(conString);
                conn.Open();

                string check = "select count(*) from Uporabnik where username='" + TextBoxUsername.Text + "'";
                SqlCommand sql = new SqlCommand(check, conn);
                int x = Convert.ToInt32(sql.ExecuteScalar().ToString());

                SqlCommand cmd2 = new SqlCommand("select admin from Uporabnik where username like @username", conn);
                cmd2.Parameters.AddWithValue("@username", TextBoxUsername.Text);
                int admin = (Int32)cmd2.ExecuteScalar();

                conn.Close();



                if (x == 1) //obstaja uporabniško ime 
                {

                    conn.Open();
                    string checkpass = "select geslo from Uporabnik where username='" + TextBoxUsername.Text + "'";
                    SqlCommand sql2 = new SqlCommand(checkpass, conn);
                    string pass = sql2.ExecuteScalar().ToString();

                   

                    
                    conn.Close();


                    //preveri če je geslo pravilno
                    if (pass == MD5Hash(TextBoxPassword.Text) && admin == 1)
                    {
                        
                            //pridobi ime in priimek iz baze
                            conn.Open();
                            string ime = "select ime from Uporabnik where username='" + TextBoxUsername.Text + "'";
                            string priimek = "select priimek from Uporabnik where username='" + TextBoxUsername.Text + "'";
                            SqlCommand sql3 = new SqlCommand(ime, conn);
                            SqlCommand sql4 = new SqlCommand(priimek, conn);
                            string imeexe = sql3.ExecuteScalar().ToString();
                            string priimekexe = sql4.ExecuteScalar().ToString();
                            conn.Close();

                            //shrani ime v sejno spremenljivko
                            Session["prijavljen"] = imeexe + " " + priimekexe;

                            Response.Write(imeexe + " " + priimekexe + "Prijava uspešna!");
                            Response.Redirect("Admin.aspx");
                        
                    }
                    else
                    {
                        Napake.Text = "Napačno geslo.";
                    }
                }

                //uporabniško ime ne obstaja
                else
                {
                    Napake.Text = "Uporabnik ne obstaja.";
                }

            }
            catch (Exception ex)
            {
                Response.Write("Napaka: " + ex.ToString());
            }
        }


    }
}