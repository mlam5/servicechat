package uni_lj.fri.is3.au3641.is3mobile;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.StringReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class ChatActivity extends Activity {
    final private String SERVICE_URL = "http://is3.somee.com/IS2/Service1.svc";
    ListView chat;
    ArrayAdapter<String> adapter;
    ArrayList<String> listItems;
    String username = "";
    String password = "";
    String lastId = "-1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ListView chat = (ListView)findViewById(R.id.chatList);
        listItems = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listItems);
        chat.setAdapter(adapter);
        username = this.getIntent().getStringExtra("username");
        password = this.getIntent().getStringExtra("password");

        final RESTCallTask rest = new RESTCallTask();
        rest.execute(SERVICE_URL + "/Messages", "Messages");
    }

    public void onSendClick(View view)
    {
        final RESTCallTask rest = new RESTCallTask();
        String msg = "";
        EditText sporocilo = ((EditText)findViewById(R.id.sporociloText));

        msg = sporocilo.getText().toString();
        try {

            msg = URLEncoder.encode(msg, "utf-8");
        }catch(Exception e){}
        //sporocilo.setText("");

        if(msg.equals(""))
            return;

        String fin = "";
        //Uri.Builder b = Uri.parse(SERVICE_URL).buildUpon();
        //b.appendPath("Send");
        //b.appendPath(username);
        //b.appendPath(sporocilo.getText().toString());
        //fin = b.build().toString();

        fin = SERVICE_URL + "/Send";
        rest.execute(fin, "Send", sporocilo.getText().toString());
    }

    public void onRefreshClick(View view)
    {
        final RESTCallTask rest = new RESTCallTask();
        rest.execute(SERVICE_URL + "/Messages/"+lastId, "Messages");
    }

    public class RESTCallTask extends AsyncTask<String, Void, String>
    {
        //private String service_url = "http://is3.somee.com/IS2/Service1.svc/Messages/%s/%s";
        private String reqType = "";

        @Override
        protected String doInBackground(String... params)
        {
            String callURL = params[0];
            reqType = params[1];

            DefaultHttpClient hc = new DefaultHttpClient();
            String result = "";

            try
            {
                if(reqType.equals("Send"))
                {
                    HttpPost request = new HttpPost(callURL);
                    request.setHeader("Authorization", username+":"+password);
                    request.setHeader("X-msg", params[2]);

                    HttpResponse response = hc.execute(request);
                    HttpEntity httpEntity = response.getEntity();
                    result = EntityUtils.toString(httpEntity);
                }
                else
                {
                    HttpGet request = new HttpGet(callURL);
                    request.setHeader("Authorization", username+":"+password);

                    HttpResponse response = hc.execute(request);
                    HttpEntity httpEntity = response.getEntity();
                    result = EntityUtils.toString(httpEntity);
                }
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }

            return result;
        }

        public class Message
        {
            public String date = "";
            public String id = "";
            public String msg = "";
            public String usr = "";

            Message(String d, String i, String m, String u)
            {
                date = d;
                id = i;
                msg = m;
                usr = u;
            }
        }

        @Override
        protected void onPostExecute(String result)
        {
            EditText editText = (EditText)findViewById(R.id.usernameText);
            //editText.setText(result);
            //Log.i("a", result);

            if(reqType.equals("Messages"))
            {
                LinkedList<Message> msgs = new LinkedList<Message>();

                for(String x : parseXml(result, "date"))
                    msgs.add(new Message(x, "", "", ""));

                int i = 0;
                for(String x : parseXml(result, "id"))
                    msgs.get(i++).id = x;

                i = 0;
                for(String x : parseXml(result, "msg"))
                    msgs.get(i++).msg = x;

                i = 0;
                for(String x : parseXml(result, "usr"))
                {
                    msgs.get(i).usr = x;
                    listItems.add(msgs.get(i).usr + ": " + msgs.get(i).msg);
                    i++;
                }

                if(!msgs.isEmpty())
                    lastId = msgs.getLast().id;

                //ListView chat = (()findViewById(R.id.chatList));
                adapter.notifyDataSetChanged();
            }
            if(reqType.equals("Send"))
            {
                for(String x:parseXml(result, "boolean"))
                {
                    if (x.equals("true"))
                    {
                        EditText sporocilo = ((EditText)findViewById(R.id.sporociloText));
                        sporocilo.setText("");
                        onRefreshClick(null);
                        break;
                    }
                }
            }
        }

        private LinkedList<String> parseXml(String input, String tag)
        {
            LinkedList<String> output = new LinkedList<String>();
            try
            {
                Document doc = convertStringToDocument(input);
                doc.getDocumentElement().normalize();

                NodeList nList = doc.getElementsByTagName(tag);

                for (int temp = 0; temp < nList.getLength(); temp++)
                {
                    Node node = nList.item(temp);

                    if (node.getNodeType() == Node.ELEMENT_NODE)
                        output.add(node.getTextContent());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return output;
        }

        private Document convertStringToDocument(String xmlStr) {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            try
            {
                builder = factory.newDocumentBuilder();
                Document doc = builder.parse( new InputSource( new StringReader( xmlStr ) ) );
                return doc;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
